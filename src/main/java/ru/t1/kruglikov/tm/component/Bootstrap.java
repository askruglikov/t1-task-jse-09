package ru.t1.kruglikov.tm.component;

import ru.t1.kruglikov.tm.api.ICommandController;
import ru.t1.kruglikov.tm.api.ICommandRepository;
import ru.t1.kruglikov.tm.api.ICommandService;
import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import ru.t1.kruglikov.tm.controller.CommandController;
import ru.t1.kruglikov.tm.repository.CommandRepository;
import ru.t1.kruglikov.tm.service.CommandService;
import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService, commandRepository);

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMAND:
                commandController.displayCommands();
                break;
            default:
                commandController.displayArgumentError();
                break;
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                commandController.displayAbout();
                break;
            case CommandConst.VERSION:
                commandController.displayVersion();
                break;
            case CommandConst.HELP:
                commandController.displayHelp();
                break;
            case CommandConst.INFO:
                commandController.displayInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case CommandConst.COMMAND:
                commandController.displayCommands();
                break;
            default:
                commandController.displayCommandError();
                break;
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

}
